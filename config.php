<?php 
	return [
		'api_url' => 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
		// DB data
		'db_host'     => 'localhost',
		'db_schema'   => 'test',
		'db_username' => 'root',
		'db_password' => 'root',
	];