What can be done better:
1. User Model can be more optimized to be general use, Model class should be created which User will extend.
2. Also there is no clearing of the params for sql injection in the model
3. View helper function to have another param for giving a string of the view (For AJAX use for example)
4. There should be also somthing like middleware to see the user step, for example not to jump to third if there is second not completed.
5. Service can be made that crieates client object and in the controller just we will use post().
6. Chain of responsability design pattern can be used here.
7. I am sure somthing can be improved for testing, I have no experience in testing(I am willing to learn)

Speed optimization:
1. Keep whole User object in SESSION, not to make query all the time.(I am keeping the user id only in session)
2. Caching some of the views
3. I don't think that there is a faster router of Macaw its really short.

Dump is in the root of the project named test_users.sql


I am really short in time, I am busy for following weekend, and I don't want to postpone the testing.

Thank you for giving me a chance. 

Regards,
Vladimir Aleksandrov





