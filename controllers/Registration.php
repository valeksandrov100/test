<?php
namespace controllers;

use models\User;
use Exception;
use GuzzleHttp\Client;

class Registration {

    public function index() {
        view('registration/index.php');
    }

    public function check() {
        if(!isset($_SESSION['user_id'])){
            redirect('/registration/stepone');
            return;
        }

        $user = User::find($_SESSION['user_id']);
        $step = $user->getStep();

        if($step == 'finished'){
            view('registration/alreadyregistred.php', ['user' => $user]);
            return;
        }

        redirect('/registration/step' . $step);
    }

    public function stepone() {
        if(requestMethod() == 'GET'){
            $user = null;

            if(isset($_SESSION['user_id'])){
                $user = User::find($_SESSION['user_id']);
            }

            view('registration/stepone.php', ['user' => $user]);
            return;    
        }

        $user = User::create($_POST);
        $_SESSION['user_id'] = $user->id;

        redirect('/registration/steptwo');
    }

    public function steptwo() {
        if(requestMethod() == 'GET'){
            $user = User::find($_SESSION['user_id']);

            view('registration/steptwo.php', ['user' => $user]);
            return;    
        }

        $user = User::find($_SESSION['user_id']);
        $user->update($_POST);

        redirect('/registration/stepthree');
    }

    public function stepthree() {
        if(requestMethod() == 'GET'){
            $user = User::find($_SESSION['user_id']);

            view('registration/stepthree.php', ["user" => $user]);
            return;    
        }

        $user = User::find($_SESSION['user_id']);
        $user->update($_POST);

        $data = array("customerId" => $user->id, "iban" => $user->iban, "owner" => $user->accountowner);
        $json_data = json_encode($data);
            
        $client = new Client();
        
        $response = $client->post(config('api_url'), [
          'body' => $json_data,
          'headers' => [
            'Accept' => 'text/plain'
          ]
        ]);

        $status = $response->getStatusCode();
        //$status = 400;

        if(substr($status, 0, 1) == "4"){
            view('registration/stepthree.php', ['error' => true, 'statuscode' => $status, 'user' => $user]);
            return;
        }
        
        $body = $response->getBody()->getContents();
        $responseData = json_decode($body, true);
        $user->update($responseData);
        
        
        redirect('/registration/stepfour');
    }

    public function stepfour() {
        $user = User::find($_SESSION['user_id']);

        view('registration/success.php', ['user' => $user]);
    }
}