<?php 

ini_set('display_errors',1 );
error_reporting(E_ALL);

session_start();

require('vendor/autoload.php');

// DB connection
global $mysqli;
$mysqli = new mysqli(config('db_host'), config('db_username'), config('db_password'), config('db_schema'));

require('routes.php');



