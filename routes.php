<?php

use NoahBuscher\Macaw\Macaw;

Macaw::get('/', 'controllers\Registration@index');
Macaw::get('/registration/check', 'controllers\Registration@check');
Macaw::any('/registration/stepone', 'controllers\Registration@stepone');
Macaw::any('/registration/steptwo', 'controllers\Registration@steptwo');
Macaw::any('/registration/stepthree', 'controllers\Registration@stepthree');
Macaw::get('/registration/stepfour', 'controllers\Registration@stepfour');
Macaw::get('/sessiondestroy', function(){
	session_destroy();
	redirect('/');
});

Macaw::dispatch();