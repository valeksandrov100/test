-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `housenumber` varchar(25) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `accountowner` varchar(100) DEFAULT NULL,
  `iban` varchar(45) DEFAULT NULL,
  `paymentDataId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (37,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','29519d9fe267411fddec5f47525f7f63ae208e4cbfaa6ed8dbd32b29b5d951d4742e9a992d5459a94570ed45633cb5e9'),(38,'Mirka','Aleksandrova','0220020','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Mirka Aleksandrova','heriban','e4258c92021375bf508f73b5355be661112c32754b8db3c3fdfa2974f1f50a3754f99964f3251940cc902048cc034d1d'),(44,'Boris','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Boris','boirsiban','7b50cf80825dbf5fb12842642b5df8b079e82b6a208a375c6e2afc3f20bcd957767696eac5e6e3fc251516881aaba038'),(45,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','da','da',NULL),(46,'Vladimir','Aleksandrov','Vlatko','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban',NULL),(47,'Vladimir','Aleksandrov','070225385',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,'Vladimir','Aleksandrov','070225385','123','Skoevska 1/2/10 Skopje','1000','Skopje','da','daa','efb26f26b3969e93b74599608cbfb2ff83f7e1958a85135368ba42a0cfc4c2d980be3d3a7d20f8a2bc8c5cf85daf26c9'),(49,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','da','myiban','eea8870336d3f9c13d07dca10dd337b80fa7b0b80e24b2f8c74eb2b0b99e97c9eb01c9479e11c726d6301a3393763c20'),(50,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','f66f3fb0ed34b7cf2b832c3d0fa0229c4152357cec312304523cd2ba714771f033503cc64ec6411e75eb6bad76ec847b'),(51,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','2c86af6e001283717aab9081cf73ec8cac268fb1b5af0146c9e16b9c8c3487a78bd198286051d96b89f867b816fd889b'),(52,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','0789fd2f7c89dbef2f76dd04434fe93c9b0b3518d4bc8c10094bba2f21158a124022317b7bad22970376167b969728c3'),(53,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','d5c201fadb2a66d5cef3703e8ac8440323718aef6b99a1de098ff064ac8b3095eff6fe47e0310dbe84305b379b517ee5'),(54,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','da','319626d72a05ef59f92fe311959ae99b8d34ef96006dafed8c39a8554a1e630bd62db1c207c9787196d8acf4c249e564'),(55,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','deba9d9d0c19d8d358d17e10a8aa6605f5bfa1f4c95f02619eaea5bb65d52239441c1e2fe7b9adb60d1f7494b9218a16'),(56,'Vladimir','Aleksandrov','070225385',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','800de45d9bb988cb89d0aa29e7c69484f963520930bef01c586a599e97cf88b6aa8635cdc95d62b9e58d74381c2f4546'),(58,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','123331','bc57aea1596101fdc347cc0c863ffd23ee893efe23383c60ab8b1c1ac7d1a46be9a379ca090c940a1831f48a50d41faf'),(59,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','123331','15c1d91be7c263c5153d57ddd55b53451651b3311f3da65d11d3bbf2aba13308d83c06d1074cda68489274c8c3688be8'),(60,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','eadba53e96b786c6fd5a900649069451ecf5ca5f70e5bd51d06d0c2c30916be9c8908169b8f236747a3a5b2e2744db92'),(61,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','bc2a44ca1c16fa9f4199a0ccd2dc8ef66c6f5c34552e9082af0de0254fc4dded1a88574aafeb26356758decd65f66c55'),(62,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','f63236b4574e972752a8152650e7195bfff47cf0890ca565b758233290038674fcf19018dd37e87d6bee925693ed394f'),(63,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','2b4b3d296d88db711406962f1c1be7d1cecd36dd34545f6b89669c97364a7f8c0d65b48a287f1820cddf924252f555fc'),(64,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','ad694f74ca9a1fbefa336666a5be87948be59fdd568b94787261e277f85c3446b88c3ccdd8a077296f2f985821278949'),(65,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','1233','6ee7f5c8b34d8c60961ac79fae1841e8f5f2fa3ce58202b468f91ade0fe16a2a3fdcb27a2d4040a5512c0df80cd2dbab'),(66,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','123331','78b3703325df68cf98bd6bc5885ab1978e8fcd194f675137fd9704f46611ffae4ad1bf84da5ecfa4ce96d8d8ff0f018b'),(67,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','89eaeb2b9aa3d8fa0c923353a6f6cda733e83b9b2fc12f2d0256e9de14167590cd81fa5312a49c4f8f16eb2831a53b6a'),(68,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','d0a568bf6509e581b8bfd402b55b74ed102fdeffd7557511f5b70674b6a02976a1b26df705beed1fc63a89b4722f867a'),(69,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','f456197e8682e18a70c9686c0abc88a7c6bb381f30c7823ea37242ff70cf2516eded49f864d4995de2f00c405d5cd9a3'),(70,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','04b2d90946cb0b7848134669c86f993e9433530030fc5229a3b11a0783e79d082921a8bdfcf53269e4c1e6c2109f8f7d'),(71,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','bc2456b617058eb4201a6f44518f8b97992510393284de999374201e630883d301ac12c6b57c9f17e7e3387446491529'),(72,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','9fb932fa33537867e09de6c4a30e9278150f7cc4b7e8426a57614cc859da144f006762c47f0cea51b3032d765df4c7cc'),(73,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','de4dcf56e6f5a2220f51260aa86de3e7e548a5b77f792c77b92d3e78d4a00bffaa751c3f8a539c7ca8ccde8b4e61d3b7'),(74,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje','Vladmir Aleksandrov','myiban','d31edd21f2f8e44a66c4ebee72037475e86185852701a0a8ccba161c366c67080a219798d2d490bc32823d4d681b9762'),(75,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje',NULL,NULL,NULL),(76,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje',NULL,NULL,NULL),(77,'Vladimir','Aleksandrov','070225385','Skoevska 1/2/10 Skopje','Skoevska 1/2/10 Skopje','1000','Skopje',NULL,NULL,NULL),(78,'Vladimir','Aleksandrov','070225385',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(79,'Vladimir','Aleksandrov','070225385','101 South Sepulveda Boulevard','101 South Sepulveda 1','90245','El Segundo','Vladmir Aleksandrov','myiban','762009919902d58a99c1f7fb785b29935c543b817492e5e828cac4a30943ef160594a27400c2349c24460e0f8c8b76c6'),(80,'Vladimir','Aleksandrov','070225385',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(81,'Vladimir','Aleksandrov','070225385',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-05 16:26:16
