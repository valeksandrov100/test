<?php namespace models;

class Base {
	protected $connection;

	public function __construct() {
		global $mysqli;
		$this->connection = $mysqli;
	}
}