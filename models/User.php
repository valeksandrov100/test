<?php namespace models;

class User extends Base {
	protected $table = 'users';

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Finds a User object by its userid
	 * @param  int $id
	 * @return User Returns found user object
	 */
	public static function find($id) {
		$obj = new self();

		$query = "SELECT * FROM users WHERE id = $id";
		$result = mysqli_query($obj->connection, $query);

		if(!$result){
			die(mysqli_error($this->connection));
		}

		if($result->num_rows == 0) {
			return NULL;
		}

		$row = $result->fetch_assoc();

		foreach($row as $columnName => $value){
			$obj->{$columnName} = $value;
		}

		return $obj;
	}

 	/**
 	 * Create a record in db as well User object
 	 * @param  array $data for the user
 	 * @return User  Created User object
 	 */
	public static function create(array $data) {
		$obj = new self;

		$query = "INSERT INTO $obj->table(". implode(',', array_keys($data)) .") VALUES (" . "'" . implode("','", array_values($data)) . "'" . ")";
		
		$result = mysqli_query($obj->connection, $query);

		if(!$result){
			die(mysqli_error($this->connection));
		}

		$id = mysqli_insert_id($obj->connection);

		$columns = $obj->getColumns();
		foreach($columns as $coulmn) {
			$obj->{$coulmn} = "";
		}

		$obj->id = $id;
		foreach($data as $key => $value) {
			$obj->{$key} = $value;
		}

		return $obj;
	}

	/**
	 * Update record
	 * @param  array  $data;
	 */
	public function update(array $data) {
		$query = "UPDATE $this->table SET ";

		$tempArr = [];
		foreach($data as $key => $value) {
			$tempArr[] = "$key = '". $value . "'";
			$this->{$key} = $value;
		}

		$query .= implode(',', $tempArr);

		$query .= " WHERE `id` = " . $this->id;

		$result = mysqli_query($this->connection, $query);

		if(!$result){
			die(mysqli_error($this->connection));
		}
	}

	// Get all columns of the users table
	public function getColumns() {
		$query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'test' AND TABLE_NAME = '" . $this->table .  "'";

		$result = mysqli_query($this->connection, $query);

		//if($result)
		$columns = [];
		while($row  = $result->fetch_assoc()){
			$columns[] = $row['COLUMN_NAME'];
		}

		return $columns;
	}

 	// Get which step is next for user based on filled inputs from db
	public function getStep() {
		if(empty($this->firstname)){
			return 'one';
		}

		if(empty($this->street)){
			return 'two';
		}

		if(empty($this->paymentDataId))
			return 'three';

		return 'finished';
	}
}
