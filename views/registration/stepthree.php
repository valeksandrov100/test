<?php include 'views/header.php' ?>

<div class="row">
	<div class="col-lg-6 offset-lg-3">
		<?php if(isset($error) && $error == true): ?>
			<div class="alert alert-danger" role="alert">
			  <strong>Oh snap!</strong> There is an error with getting paymentId, status code <?= $statuscode ?>
			</div>
		<?php endif; ?>

		<h3>Insert payment information</h3>
		<form method="POST" action="/registration/stepthree">
			<div class="form-group">
				<label>Account owner: </label>
				<input type="text" class="form-control" name="accountowner" required value="<?= isset($user) ? $user->accountowner : '' ?>"/>
			</div>

			<div class="form-group">
				<label>IBAN: </label>
				<input type="text" class="form-control" name="iban" required value="<?= isset($user) ? $user->iban : "" ?>" />
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">NEXT</button>
				<a href="/" class="btn btn-danger float-right">Leave Registration Process</a>
			</div>
		</form>

		<div class="progress">
		  <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
		</div>
	</div>
</div>

<?php include 'views/footer.php' ?>