<?php include 'views/header.php' ?>

<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Well done!</h4>
    <p>You have successfully registred to our system and your Payment Data Id is <?= $user->paymentDataId ?></p>
  <hr>
  <p class="mb-0">
  	<div class="progress">
		  <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
	</div>
  </p>
</div>

<?php include 'views/footer.php';