<?php include 'views/header.php' ?>

<div class="row">
	<div class="col-lg-6 offset-lg-3">
		<h3>Insert address information</h3>
		<form method="POST" action="/registration/steptwo">
			<div class="form-group">
				<label>Street: </label>
				<input type="text" class="form-control" name="street" required value="<?= $user->street ?>" />
			</div>

			<div class="form-group">
				<label>Housenumber: </label>
				<input type="text" class="form-control" name="housenumber" required value="<?= $user->housenumber ?>" />
			</div>
			
			<div class="form-group">
				<label>Zip: </label>
				<input type="text" class="form-control" name="zip" required value="<?= $user->zip ?>" />
			</div>

			<div class="form-group">
				<label>City: </label>
				<input type="text" class="form-control" name="city" required value="<?= $user->city ?>" />
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">NEXT</button>
				<a href="/" class="btn btn-danger float-right">Leave Registration Process</a>
			</div>

			<div class="progress">
			  <div class="progress-bar w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
			</div>
		</form>
	</div>
</div>

<?php include 'views/footer.php' ?>