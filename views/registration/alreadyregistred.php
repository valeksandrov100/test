<?php include 'views/header.php' ?>

<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">Already Registred!</h4>
    <p>You are already registred in our system and your Payment Data Id is <?= $user->paymentDataId ?></p>
  <hr>
</div>

<?php include 'views/footer.php';