<?php include 'views/header.php' ?>

<div class="row">
	<div class="col-lg-6 offset-lg-3">
		<h3>Insert personal information</h3>
		<form method="POST" action="/registration/stepone" class="form-horizontal">
			<div class="form-group">
				<label for="firstname">Firstname: </label>
				<input id="firstname" type="text" class="form-control" name="firstname" required value="<?= isset($user) ? $user->firstname : '' ?>" />
			</div>

			<div class="form-group">
				<label>Lastname: </label>
				<input type="text" class="form-control" name="lastname" required value="<?= isset($user) ? $user->lastname : '' ?>" />
			</div>
			
			<div class="form-group">
				<label>Telephone: </label>
				<input type="text" class="form-control" name="telephone" required value="<?= isset($user) ? $user->telephone : '' ?>" />
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">NEXT</button>
				<a href="/" class="btn btn-danger float-right">Leave Registration Process</a>
			</div>
		</form>

		<div class="progress">
		  <div class="progress-bar w-25" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
		</div>
	</div>
</div>

<?php include 'views/footer.php' ?>